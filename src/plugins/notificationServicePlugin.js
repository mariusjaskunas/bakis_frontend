import notificationService from "../services/NotificationService";

export default {
  install(Vue) {
    Vue.prototype.$toasts = notificationService;
    Vue.toasts = notificationService;
  }
};
