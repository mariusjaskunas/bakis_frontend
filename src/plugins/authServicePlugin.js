import authenticationService from "../services/AuthenticationService";

export default {
  install(Vue) {
    Vue.prototype.$auth = authenticationService;
    Vue.auth = authenticationService;
  }
};
