import apiService from "../services/ApiService";

export default {
  install(Vue) {
    Vue.prototype.$api = apiService;
    Vue.api = apiService;
  }
};
