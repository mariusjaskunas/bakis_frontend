import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import draggable from "vuedraggable";
import Vuelidate from "vuelidate";
import Notifications from "vue-notification";
import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import apiServicePlugin from "./plugins/apiServicePlugin";
import authServicePlugin from "./plugins/authServicePlugin";
import notificationServicePlugin from "./plugins/notificationServicePlugin";
import VueLoading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";

Vue.config.productionTip = false;

Vue.use(VueLoading);
Vue.use(Notifications);
Vue.use(notificationServicePlugin);
Vue.use(apiServicePlugin);
Vue.use(authServicePlugin);
Vue.use(Vuelidate);
Vue.component("draggable", draggable);
Vue.use(VueMaterial);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
