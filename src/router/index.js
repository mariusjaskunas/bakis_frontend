import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Services from "../views/Services.vue";
import Login from "../views/Login";
import Users from "../views/Users";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    path: "/connections",
    name: "Home",
    component: Home,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/services",
    name: "Services",
    component: Services,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/users",
    name: "Users",
    component: Users,
    meta: {
      requiresAuth: true,
      admin: true
    }
  },
  {
    path: "*",
    name: "Login redirected",
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    path: "*",
    name: "Home redirected",
    component: Home,
    meta: {
      requiresAuth: true
    }
  }
];

const linkActiveClass = "route-active";

const router = new VueRouter({
  routes,
  linkActiveClass
});

router.beforeEach((to, from, next) => {
  const user = Vue.auth.getUser();
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (user) {
      if (
        JSON.parse(user).role !== 1 &&
        to.matched.some(record => record.meta.admin)
      ) {
        next({ name: "Home" });
      } else {
        next();
      }
    } else {
      next({ name: "Login" });
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (user) {
      next({ name: "Home" });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
